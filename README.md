**Sample Bitbucket Pipelines Configuration using the Salesforce DX CLI**

This repository contains an example of a bitbucket-pipelines.yml file for building Salesforce using the Salesforce DX CLI (sfdx-cli).

This example uses the docker image in https://hub.docker.com/r/salestrip/sfdx-cli/ or https://bitbucket.org/SalesTrip/docker-sfdx-cli which contains a node.js installation of sfdx-cli. It then authenticates the CLI with your Dev Hub org

The flow uses JWT to authorise with a Dev Hub in order to create scratch orgs. Instructions for setting up JWT are found here: https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_jwt_flow.htm#sfdx_dev_auth_jwt_flow

The following environment variables need to be configured:

* CLIENT_ID: Client Id of your connected app. Instructions here: https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_connected_app.htm
* PRIVATE_KEY: Base64 encoded version of you private key from the connected app created in your Dev Hub org. Instructions here: https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_key_and_cert.htm
* USERNAME: Username of a user which is authorised to use the connected app and scratch orgs in your Dev Hub org.
